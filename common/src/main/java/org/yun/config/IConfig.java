package org.yun.config;


import java.util.List;

/**
 * @author liyunfeng31
 */
public interface IConfig {


    /**
     * 获取全部配置
     * @return activity
     */
    String configs();


    /**
     * 发布配置
     * @param keyVal 配置 key=val
     */
    void publishConfig(String keyVal);

    /**
     * 获取活动信息
     * @return activity
     */
    String getActivity(Long lotteryId);

    /**
     * 获取快照开关
     * @return true开  false关
     */
    boolean getSnapshotSwitch();


    /**
     * 获取异步开关
     * @return true开  false关
     */
    boolean getAsynSwitch();


    /**
     * 获取售罄的sku列表
     * @return 获取sku库存为空的list
     */
    <T> List <T> getNoStockSkuList(Class<T> c);

}
