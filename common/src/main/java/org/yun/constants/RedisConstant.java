package org.yun.constants;


/**
 * @author liyunfeng31
 */
public class RedisConstant {

    /**
     * 预约标识
     */
    public static final String APPOINTMENT = "appointment";


    /**
     * redis黑名单
     */
    public static final String BLACKLIST = "blacklist";


    /**
     * 预约名单前缀
     */
    public static final String APPOINT_UUID_IDS = "appointUuid:";


    /**
     * 售罄skuList前缀
     */
    public static final String NO_STOCK_SKU_LIST = "nostockskulist=";


    /**
     * 订单快照前缀
     */
    public static final String SNAPSHOT = "snapshot:";


    /**
     * 活动商品前缀
     */
    public static final String ACTIVITY_SKU = "activity:sku:";


    /**
     * 库存前缀
     */
    public static final String STOCK = "stock:";


    /**
     * 参加过的用户IDS前缀
     */
    public static final String USER_IDS = "userIds:";

    /**
     * 用户抽奖机会前缀
     */
    public static final String CHANCE_UID = "lty_chance_uid:";

    /**
     * 奖品ID前缀 约等于sku
     */
    public static final String PRIZE_ID = "prizeId:";

    /**
     * 奖品-券码cache list前缀
     */
    public static final String COUPON_LIST = "coupon:prizeId:";

    /**
     * 奖品-券码补货前缀
     */
    public static final String COUPON_REPLENISH_LIST = "replenish:prizeId:";


    /**
     * 奖品ID前缀 约等于sku
     */
    public static final String REPLENISH_LOCK = "replenish:prizeId:";



    /**
     * 红包通用前缀
     */
    public static final String RED_PACK = "redPack:";


    /**
     * 红包领取用户集合
     */
    public static final String RP_USER = "_user";

    /**
     * 红包领取记录
     */
    public static final String RP_RECORD = "_record";


    /**
     * 推送发布无货到配置中心的锁
     */
    public static final String PUBLISH_LOCK = "PUBLISH_NO_STOCKS_LOCK_";


    /**
     * 任意字符串做val
     */
    public static final String VAL = "1";
}
