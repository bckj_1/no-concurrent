package org.yun.util;

import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentityGenerator;

import java.io.Serializable;

/**
 * @author liyunfeng31
 */
public class IDUtil extends IdentityGenerator {

    private static final SnowFlake SNOW_FLAKE_GENERATOR = new SnowFlake();

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws MappingException {
        Serializable id =  SNOW_FLAKE_GENERATOR.next();
        if (id != null) {
            return id;
        }
        return super.generate(session, object);
    }

    public static Long id(){
        return SNOW_FLAKE_GENERATOR.next();
    }

}