package org.yun.exception;


import java.io.Serializable;


/**
 * @author liyunfeng31
 */
public class ErrorCode implements Serializable {

    public static final int SUCCESS_CODE = 1;


    public enum LOTTERY implements IErrorCode{

        /**
         * 无票异常
         */
        NO_TICKET(1003, "抽奖次数已用光"),
        LOTTERY_STATUS_ERROR(1004, "活动不存在或者已下线");

        private int code;

        private String defaultMessage;

        LOTTERY(int code, String defaultMessage) {
            this.code = code;
            this.defaultMessage = defaultMessage;
        }



        @Override
        public String getDefaultMsg() {
            return defaultMessage;
        }

        @Override
        public int getCode() {
            return code;
        }
    }



    public enum RED_PACK implements IErrorCode{

        /**
         * 重复参加
         */
        PARTICIPATED(2001, "活动未开始或已下架"),
        NO_STOCK(2002, "库存不足"),
        NUM_TOO_FEW(2003, "红包数过少"),
        AMOUNT_TOO_FEW(2004, "金额太少不足以分配"),
        ;

        private int code;

        private String defaultMessage;

        RED_PACK(int code, String defaultMessage) {
            this.code = code;
            this.defaultMessage = defaultMessage;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getDefaultMsg() {
            return defaultMessage;
        }

    }




    public enum SEC_KILL implements IErrorCode{

        /**
         * 活动不存在
         */
        NO_EXIST(3001, "活动不存在"),
        NO_ONLINE(3002, "活动未上线"),
        NO_STARTED(3003, "活动未开始"),
        OVER(3004, "活动已结束"),
        NO_STOCK(3005, "库存不足"),
        PARTICIPATED(3006, "重复参加"),
        ;

        private int code;

        private String defaultMessage;

        SEC_KILL(int code, String defaultMessage) {
            this.code = code;
            this.defaultMessage = defaultMessage;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getDefaultMsg() {
            return defaultMessage;
        }

    }


}
