package org.yun.exception;

import java.io.Serializable;


/**
 * @author liyunfeng31
 */
public interface IErrorCode extends Serializable {


    String getDefaultMsg();

    int getCode();
}
