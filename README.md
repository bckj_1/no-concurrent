# noConcurrent
1. 没有高并发是加1000台机器解决不了的，如果有，那就加100000台
2. 限流永远是第一位的，nginx limit conn/req限流，网关令牌桶限流，tomcat和业务web限流和拒绝/丢弃策略，连接池限流
3. 最容易达到瓶颈的通常是db，因为它最不方便扩容。使用高性能的中间件如redis，一定避免直接冲击db
4. 预加载和异步是防止高并发的有效手段。典型的是缓存预热和MQ异步存储

#### 介绍
1. 抽奖，抢红包，秒杀 3个实际案例
2. no more concurrent!

#### 抽奖架构
关键词：预加载，库存池
![binaryTree](抽奖流程图.png)
#### 抢红包架构
关键词：实时/预加载2种策略
![binaryTree](抢红包.jpg)
#### 秒杀架构
关键词：策略, limit, redis lua, config center
![binaryTree](秒杀.jpg)

