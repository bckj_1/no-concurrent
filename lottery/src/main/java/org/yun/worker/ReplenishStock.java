package org.yun.worker;


/**
 * @Description: 异步补货接口
 * @author liyunfeng31
 */
public interface ReplenishStock {


    /**
     * 执行补货
     * @param prizeId 奖品ID
     * @param num num
     * @return codePwd
     */
    int doReplenishStock(Long prizeId, int num);
}
