package org.yun.biz.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.yun.biz.model.Record;


/**
 * @ProjectName: no-concurrent
 * @ClassName: StockDao
 * @Description: record
 * @Author: liyunfeng31
 * @Date: 2020/10/5 0:42
 */
public interface RecordRepository extends JpaRepository<Record, Long> {

}