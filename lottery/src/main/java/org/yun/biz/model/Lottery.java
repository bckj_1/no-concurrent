package org.yun.biz.model;

import lombok.Data;
import org.yun.biz.dto.PrizeDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @author liyunfeng31
 */
@Data
@Entity
@Table(name = "lottery")
public class Lottery implements Serializable {

    /**
     * 抽奖活动ID
     */
    @Id
    @Column(name = "lottery_id")
    private Long lotteryId;

    /**
     * 活动主题
     */
    @Column(name = "topic")
    private String topic;

    /**
     * 活动状态 0默认  1上线  2下线
     */
    @Column(name = "state")
    private Integer state;

    /**
     * 活动链接
     */
    @Column(name = "link")
    private String link;

    /**
     * 活动图片
     */
    @Column(name = "images")
    private String images;


    /**
     * 开始时间
     */
    @Column(name = "start_time")
    private Integer startTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Integer endTime;


    /**
     * 创建时间
     */
    @Column(name = "c_t")
    private Date ct;

    /**
     * 奖品List
     */
    @Transient
    private List<Prize> prizes;
}
