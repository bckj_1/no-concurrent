package org.yun.biz.model;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liyunfeng31
 */
@Data
@Entity
@Table(name = "lottery_record")
public class Record implements Serializable {

    /**
     * 记录ID
     */
    @Id
    @Column(name = "record_id")
    private Long recordId;


    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * 活动主题
     */
    @Column(name = "lottery_topic")
    private String lotteryTopic;

    /**
     * 券码
     */
    @Column(name = "code")
    private String code;

    /**
     * 卡密
     */
    @Column(name = "pwd")
    private String pwd;


    /**
     * 奖品ID
     */
    @Column(name = "prize_id")
    private Long prizeId;


    /**
     * 奖品名称
     */
    @Column(name = "prize_name")
    private String prizeName;


    /**
     * 抽奖活动ID
     */
    @Column(name = "lottery_id")
    private Long lotteryId;

    /**
     * 奖品类型
     */
    @Column(name = "prize_type")
    private Integer prizeType;

    /**
     * 图片
     */
    @Column(name = "images")
    private String images;

    /**
     * 位置
     */
    @Column(name = "sort")
    private Integer sort;

    /**
     * 中奖率
     */
    @Column(name = "ratio")
    private BigDecimal ratio;

    /**
     * 总库存
     */
    @Column(name = "total_stock")
    private Integer totalStock;

    /**
     * 可用库存
     */
    @Column(name = "valid_stock")
    private Integer validStock;

    /**
     * 状态
     */
    @Column(name = "state")
    private Integer state;

    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;


    /**
     * 有效兑换开始时间
     */
    @Column(name = "exchange_st")
    private Integer exchangeSt;

    /**
     * 有效兑换结束时间
     */
    @Column(name = "exchange_et")
    private Integer exchangeEt;

    /**
     * 生效时间
     */
    @Column(name = "effective_time")
    private Integer effectiveTime;

    /**
     * 过期失效时间
     */
    @Column(name = "expiry_time")
    private Integer expiryTime;


    @Column(name = "c_t")
    private Date ct;

}
