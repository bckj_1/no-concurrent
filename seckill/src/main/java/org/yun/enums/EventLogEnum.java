package org.yun.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author liyunfeng31
 */

@Getter
@AllArgsConstructor
public enum  EventLogEnum {
    /**
     * 事件Log枚举
     */
    SYNC_STOCK_CACHE_MISS("同步库存缓存为空",31),
    SYNC_STOCK_CACHE_DIFF("同步库存缓存不一致",32),
    SYNC_STOCK_CACHE_RESULT("同步库存结果",33);

    private String name;
    private int code;

}
