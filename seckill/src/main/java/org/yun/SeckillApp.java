package org.yun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @ProjectName: no-concurrent
 * @ClassName: SeckillApp
 * @Description: 秒杀系统
 * @Author: liyunfeng31
 * @Date: 2020/10/4 21:40
 */
@EnableTransactionManagement
@SpringBootApplication
public class SeckillApp {

    public static void main(String[] args) {
        SpringApplication.run(SeckillApp.class, args);
    }
}
