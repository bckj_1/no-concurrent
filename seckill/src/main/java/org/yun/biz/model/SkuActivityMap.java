package org.yun.biz.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author liyunfeng31
 */
@Data
@Entity
@Table(name = "sku_activity_map")
public class SkuActivityMap implements Serializable {

    @Id
    private Long id;

    private Long skuId;

    private Long activityId;


}
