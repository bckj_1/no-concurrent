package org.yun.biz.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.yun.biz.model.Order;
import org.yun.biz.model.Stock;


/**
 * @ProjectName: no-concurrent
 * @ClassName: OrderRepository
 * @Description: 订单dao
 * @Author: liyunfeng31
 * @Date: 2020/10/5 0:42
 */
public interface OrderRepository extends JpaRepository<Order, Long> {

}