package org.yun.biz.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.yun.biz.model.EventLog;


/**
 * @ProjectName: no-concurrent
 * @ClassName: EventLogRepository
 * @Description: 系统事件日志
 * @Author: liyunfeng31
 * @Date: 2020/10/15 13:22
 */
@Repository
public interface EventLogRepository extends JpaRepository<EventLog, Long> {

}