package org.yun.biz.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.yun.biz.model.SkuActivityMap;


/**
 * @ProjectName: no-concurrent
 * @ClassName: SkuActivityMapRepository
 * @Description: sku-activity映射
 * @Author: liyunfeng31
 * @Date: 2020/10/6 13:02
 */
@Repository
public interface SkuActivityMapRepository extends JpaRepository<SkuActivityMap, Long> {

}