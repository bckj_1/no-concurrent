package org.yun.biz.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.Version;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @ProjectName: no-concurrent
 * @ClassName: RedPack
 * @Description: 红包实体类
 * @Author: liyunfeng31
 * @Date: 2020/10/5 0:09
 */

@Data
@Entity
@Table(name = "red_pack")
public class RedPack implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "custom-id")
    @GenericGenerator(name = "custom-id", strategy = "org.yun.util.IDUtil")
    @Column(name = "red_pack_id")
    private Long redPackId;

    @Column(name = "title")
    private String title;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "num")
    private Integer num;

    @Column(name = "remaining_amount")
    private Integer remainingAmount;

    @Column(name = "remaining_num")
    private Integer remainingNum;

    @Column(name = "c_t")
    private Date ct;


    /**
     * 群ID
     */
    @Column(name = "group_id")
    private Long groupId;
}
