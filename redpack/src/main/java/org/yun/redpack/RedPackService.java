package org.yun.redpack;

import org.yun.biz.model.RedPack;

/**
 * @ProjectName: no-concurrent
 * @ClassName: RedPackService
 * @Description: 红包接口
 * @Author: liyunfeng31
 * @Date: 2020/10/4 23:19
 */
public interface RedPackService {


    /**
     * 创建红包
     * @param redPack red
     * @param userId userId
     */
    Long createRedPack(RedPack redPack, Long userId);


    /**
     * 点击红包/拆红包
     * @param redPackId redID
     * @param userId userId
     * @return 红包信息
     */
    boolean clickRedPack(Long redPackId, Long userId);

    /**
     * 抢红包
     * @param redPackId redPackId
     * @param userId userId
     * @return redPack
     */
    Integer grabRedPack(Long redPackId, Long userId);

}
