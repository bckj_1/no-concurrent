package org.yun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @ProjectName: no-concurrent
 * @ClassName: RedPackApp
 * @Description: 抢红包
 * @Author: liyunfeng31
 * @Date: 2020/10/5 22:10
 */
@EnableTransactionManagement
@SpringBootApplication
public class RedPackApp {

    public static void main(String[] args) {
        SpringApplication.run(RedPackApp.class, args);
    }
}
